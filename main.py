import infrastructure.Elastic_domain as elastic_domain
import interfaces.CanadienFoodFileResource as FoodResource
import xlrd

from datetime import datetime
from elasticsearch import Elasticsearch, helpers

import infrastructure.FoodIndexer as FoodIndexer

""" NB: You must start ElasticSearch before using this code """


""" All endpoint URLs """
base = "https://food-nutrition.canada.ca/api/"

#Canadian Nutrient (CNF) API
#CNF_FOOD_URL = base + "canadian-nutrient-file/food/?lang=fr&type=json&id=1256"
# A food cannot be further decomposed into ingredients.    --------------------------------------------------
CNF_FOOD_URL = base + "canadian-nutrient-file/food/?lang=fr&type=json"
CNF_NUTRIENT_NAME_URL = base + "canadian-nutrient-file/nutrientname/?lang=fr&type=json"
CNF_NUTRIENT_GROUP_URL = base + "canadian-nutrient-file/nutrientgroup/?lang=fr&type=json"
CNF_NUTRIENT_SOURCE_URL = base + "canadian-nutrient-file/nutrientsource/?lang=fr&type=json"
#CNF_NUTRIENT_AMOUNT_URL = base + "canadian-nutrient-file/nutrientamount/?type=json&lang=fr&id=1256"
#Identify the nutrient amount per 100 grams for a food. ----------------------------------------------------
CNF_NUTRIENT_AMOUNT_URL = base + "canadian-nutrient-file/nutrientamount/?type=json&lang=fr"
CNF_SERVING_SIZE_URL = base + "canadian-nutrient-file/servingsize/?type=json&lang=fr"
CNF_REFUSE_AMOUNT_URL = base + "canadian-nutrient-file/refuseamount/?lang=fr&type=json"
CNF_YIELD_AMOUNT_URL = base + "canadian-nutrient-file/yieldamount/?lang=fr&type=json"

food_resource = FoodResource.CanadienFoodFileResource()
foodIndexer = FoodIndexer.FoodIndexer()


def make_a_query(query_field_name, query_field_path, query_string="céréale", number_of_items_to_return=5):
    query = {
        "aggs": {
            "Food_desc_aggr": {
                "terms": {
                    "field": "food_description.keyword",
                    "size": number_of_items_to_return
                },
                "aggs": {
                    "nutrients_nested_aggregation": {
                        "nested": {
                            "path": "nutrients"
                        },
                        "aggs": {
                            query_field_name: { # Must be aggregation name ---------------------------------------------
                                "terms": {
                                    "field": query_field_path,
                                    "size": 1000
                                },
                                "aggs": {
                                    "Total_nutrients_values": {
                                        "sum": {
                                            "field": "nutrients.nutrient_value"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "query": {
            "match": {
                "food_description": query_string
            }
        }
    }

    res = elastic_client.search(index="foods_enriched",
                                body=query, size=10,
                                track_total_hits=True)
    # print(res['hits']['hits'][0]['_source']['nutrients'])
    aggregation_results = res['aggregations']['Food_desc_aggr']['buckets']

    """
    for aggr_result in aggregation_results:
        print(aggr_result['key'])
        for nutrient_group in aggr_result['nutrients_nested_aggregation']['Nutrient_groups']['buckets']:
            print("Nutrient group (", nutrient_group['key'], '):    Total Nutrient value(',
                  nutrient_group['Total_nutrients_values']['value'], ')')
        print()
    """

    # res = elastic_client.search(index="nutrients_enriched",
    #                            body={"query": {"match": {"food.food_description": "céréale"}}}, size=2,
    #                            track_total_hits=True)
    # print("Got %d Hits:" % res['hits']['total']['value'])
    # for hit in res['hits']['hits']:
    # print("%(food.food_description)s:  %(name.nutrient_group.nutrient_group_name)s %(name.nutrient_name)s %(nutrient_value)s %(name.unit)s %(source.nutrient_source_description)s" % hit["_source"])
    # print("nutrient_name: ", res['hits']['hits'][0]['_source'])
    return aggregation_results


def compute_score(aggregation_results):
    # Nutrient groups Weights
    weights = {
        "Lipides_weight": 0.1,
        "Acides_aminés_weight": 0.2,
        "Vitamines_weight": 0.4,
        "Minéraux_weight": 0.3,
        "Macronutriments_weight": 0.2,
        "Autre_composantes_weight": 0.1,
        "Autres_glucides_weight": 0.1
    }

    #weight_env = {}
    #weight_nutri = {}

    aliment0 = aggregation_results[0]
    score = 0
    for nutrient_group, weight in zip(aliment0['nutrients_nested_aggregation']['Nutrient_groups']['buckets'], weights.values()):
        score +=  nutrient_group['Total_nutrients_values']['value'] * weight

    print("The score aliment0: ", score)

    """
    aliment1 = aggregation_results[1]
    score = 0
    for nutrient_group, weight in zip(aliment1['nutrients_nested_aggregation']['Nutrient_groups']['buckets'],
                                      weights.values()):
        score += nutrient_group['Total_nutrients_values']['value'] * weight

    print("The score aliment1: ", score)

    aliment2 = aggregation_results[2]
    score = 0
    for nutrient_group, weight in zip(aliment2['nutrients_nested_aggregation']['Nutrient_groups']['buckets'],
                                      weights.values()):
        score += nutrient_group['Total_nutrients_values']['value'] * weight

    print("The score aliment2: ", score)
    """

def get_protegez_vous_nutrients_categories(file_path="../../Comparateur/Protegez-Vous_Céréales.xlsx"):
    workbook = xlrd.open_workbook(file_path)
    ingredient_worksheet = workbook.sheet_by_name('Ingrédients')

    row_number = 2

    # First and last indexes
    all_nutrient_indexes = {
        "securitaire_naturel" : (5, 143),
        "securitaire_non_naturel" : (145, 264),
        "risque_probable_naturel" : (266, 269),
        "risque_probable_non_naturel" : (271, 290),
        "a_reduire_naturel" : (292, 317),
        "a_reduire_non_naturel" : (319, 346),
        "a_eviter_pour_certains_naturel" : (348, 362),
        "a_eviter_pour_certains_non_naturel" : (364, 426),
        "a_eviter_naturel" : (428, 437),
        "a_eviter_non_naturel" : (439, 478)
    }

    nutrient_categories_and_their_contents = dict();
    for nutrient_category, indexes in all_nutrient_indexes.items():
        nutrient_categories_and_their_contents[nutrient_category] = list()
        for column_number in range(indexes[0], indexes[1] + 1):
            nutrient_categories_and_their_contents[nutrient_category].append(ingredient_worksheet.cell(row_number, column_number).value)

    return nutrient_categories_and_their_contents

def compute_score(ingredients_list, nutrient_categories_and_their_contents):
    ingredient_penalties = {
        "securitaire_naturel": 0,
        "securitaire_non_naturel": 2,
        "risque_probable_naturel": 5,
        "risque_probable_non_naturel": 10,
        "a_reduire_naturel": 5,
        "a_reduire_non_naturel": 10,
        "a_eviter_pour_certains_naturel": 3,
        "a_eviter_pour_certains_non_naturel": 6,
        "a_eviter_naturel": 6,
        "a_eviter_non_naturel": 12
    }

    for ingredient in ingredients_list:
        total_ingredients_securitaire_naturel = 0
        total_ingredients_securitaire_non_naturel = 0
        total_ingredients_risque_probable_naturel = 0
        total_ingredients_risque_probable_non_naturel = 0
        total_ingredients_a_reduire_naturel = 0
        total_ingredients_a_reduire_non_naturel = 0
        total_ingredients_a_eviter_pour_certains_naturel = 0
        total_ingredients_a_eviter_pour_certains_non_naturel = 0
        total_ingredients_a_eviter_naturel = 0
        total_ingredients_a_eviter_non_naturel = 0

        for nutrient_category in nutrient_categories_and_their_contents:
            print(nutrient_category)
            #if ingredient in nutrient_categor

if __name__ == "__main__":
    elastic_client = Elasticsearch(hosts=[elastic_domain.domain_address])

    # Test health
    r = elastic_client.cluster.health(wait_for_status='yellow', request_timeout=1)
    print(f"Status of '{r['cluster_name']}': {r['status']}")

    # Get resources as lists of jsons
    """
    print("Fetching nutrient names ...")
    nutrient_name_response, nutrient_name_jsons = food_resource.get_all_json_document(CNF_NUTRIENT_NAME_URL)
    print("Fetching nutrient sources ...")
    nutrient_source_response, nutrient_source_jsons = food_resource.get_all_json_document(CNF_NUTRIENT_SOURCE_URL)
    print("Fetching nutrient groups ...")
    nutrient_group_response, nutrient_group_jsons = food_resource.get_all_json_document(CNF_NUTRIENT_GROUP_URL)
    print("Fetching nutrient amounts ...")
    nutrient_amount_response, nutrient_amount_jsons = food_resource.get_all_json_document(CNF_NUTRIENT_AMOUNT_URL)
    print("Fetching foods ...")
    food_response, food_jsons = food_resource.get_all_json_document(CNF_FOOD_URL)
    print("Fetching serving sizes ...")
    serving_size_response, serving_size_jsons = food_resource.get_all_json_document(CNF_SERVING_SIZE_URL)
    print("Fetching refuse amounts ...")
    refuse_amount_response, refuse_amount_jsons = food_resource.get_all_json_document(CNF_REFUSE_AMOUNT_URL)
    print("Fetching yield amounts ...")
    yield_amount_response, yield_amount_jsons = food_resource.get_all_json_document(CNF_YIELD_AMOUNT_URL)
    """

    # Index the resources gotten
    #foodIndexer.index_documents(nutrient_name_jsons, elastic_client, "nutrient_names")
    #foodIndexer.index_documents(nutrient_source_jsons, elastic_client, "nutrient_sources")
    #foodIndexer.index_documents(nutrient_group_jsons, elastic_client, "nutrient_groups")
    #foodIndexer.index_documents(nutrient_amount_jsons, elastic_client, "nutrient_amounts")
    #foodIndexer.index_documents(food_jsons, elastic_client, "foods")
    #foodIndexer.index_documents(serving_size_jsons, elastic_client, "serving_size")
    #foodIndexer.index_documents(refuse_amount_jsons, elastic_client, "refuse_amount")
    #foodIndexer.index_documents(yield_amount_jsons, elastic_client, "yield_amount")

    # Make a query
    query_field_nutrients_groups = "nutrients.name.nutrient_group.nutrient_group_name.keyword"
    query_field_nutrients = "nutrients.name.nutrient_name.keyword"
    aggregation_results = make_a_query("nutrient_names", query_field_nutrients, query_string="céréale", number_of_items_to_return=5)

    # Compute score
    #compute_score(aggregation_results)


    # Get Protégez-vous ingrédients catégories
    returned_nutrient_categories_and_their_contents = get_protegez_vous_nutrients_categories()

    # Get food score
    #compute_score()



    for category in returned_nutrient_categories_and_their_contents.keys():
        if "ACIDE ASPARTIQUE" in returned_nutrient_categories_and_their_contents[category]:
            print("yes")