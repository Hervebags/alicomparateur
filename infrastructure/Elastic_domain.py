""" Elastic Search domain address """
IP_address = "localhost"
port_number = "9200"
domain_address = IP_address + ":" + port_number
