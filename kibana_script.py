#################################################################
# Update documents with specified fields
#################################################################
PUT test

PUT test/_doc/1
{
  "counter" : 1,
  "tags" : ["red"]
}

GET test/_search

POST test/_update/1
{
  "script": {
    "source": "ctx._source.new_field = 'Hello World'"
  }
}

GET test/_search


POST _ingest/pipeline/_simulate
{
  "pipeline": {
      "description": "",
      "processors": [
        {
          "split": {
            "field": "message",
            "separator": " "
          }
        }
      ]
  },
  "docs": [
    {
      "_source":{
        "message": "2019-09-29 AppServer1 STATUS_OK"
      }
    }
  ]
}
#################################################################
# Aggregation
#################################################################
GET kibana_sample_data_logs/_search
{
  "track_total_hits": true
}

POST /kibana_sample_data_ecommerce/_search?size=0
{
    "aggs": {
      "avg_grade": {
        "avg": {
          "field": "taxful_total_price"
        }
      }
    }
}

GET kibana_sample_data_ecommerce/_search
{
  "size": 0,
  "aggs": {
    "unique_sku": {
      "cardinality": {
        "field": "sku"
      }
    }
  }
}

PUT testing_nested_aggregation_index
{
  "mappings": {
    "properties": {
      "Employee": {
        "type": "nested",
        "properties" : {
           "first" : { "type" : "text" },
           "last" : { "type" : "text" },
          "salary" : { "type" : "double" }
        }
      }
    }
  }
}

PUT testing_nested_aggregation_index/_doc/1
{
  "group" : "Logz",
  "Employee" : [
    {
      "first" : "Ana",
      "last" :  "Roy",
      "salary" : "70000" 
    },
    {
      "first" : "Jospeh",
      "last" :  "Lein",
      "salary" : "64000" 
    },
     {
      "first" : "Chris",
      "last" :  "Gayle",
      "salary" : "82000" 
    },
    {
      "first" : "Brendon",
      "last" :  "Maculum",
      "salary" : "58000" 
    },
    {
      "first" : "Vinod",
      "last" :  "Kambli",
      "salary" : "63000" 
    },
     {
      "first" : "DJ",
      "last" :  "Bravo",
      "salary" : "71000" 
    },
    {
      "first" : "Jaques",
      "last" :  "Kallis",
      "salary" : "75000" 
    }]}

GET /testing_nested_aggregation_index/_search
{
  "size": 0, 
  "aggs": {
    "My_Nested_Aggregation": {
      "nested": {
        "path": "Employee"
      },
      "aggs": {
        "Min_Salary": {
          "min": {
            "field": "Employee.salary"
          }
        }
      }
    }
  }
}


POST foods_enriched/_search
{
  "size": 0,
  "aggs": {
    "Food_desc_aggr": {
      "terms": {
        "field": "food_description.keyword",
        "size": 20
      },
      "aggs": {
        "nutrients_nested_aggregation": {
          "nested": {
            "path": "nutrients"
          },
          "aggs": {
            "Nutrient_groups": {
              "terms": {
                "field": "nutrients.name.nutrient_group.nutrient_group_name.keyword",
                "size": 10
              },
              "aggs": {
                "Total_nutrients_values": {
                  "sum": {
                    "field": "nutrients.nutrient_value"
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "query": {
    "match": {
      "food_description": "céréale"
    }
  }
}

GET foods/_search
#################################################################
# Ali comparateur
#################################################################
GET test/_search
GET nutrient_names/_search
GET nutrient_sources/_search
GET nutrient_groups/_search
GET nutrient_amounts/_search
{
  "track_total_hits": true
}
#################################################################
# Enrich nutrient_names with:
# nutrient_groups
#################################################################
PUT /_enrich/policy/nutrient_groups_policy
{ 
  "match": {
    "indices": [
      "nutrient_groups"
    ],
    "match_field": "nutrient_group_id",
    "enrich_fields": [
      "nutrient_group_name",
			"nutrient_group_order"
    ]
  }
}
POST /_enrich/policy/nutrient_groups_policy/_execute

PUT _ingest/pipeline/nutrient_groups_pipeline
{ 
  "processors": [
    {
      "enrich": {
        "policy_name": "nutrient_groups_policy",
        "field": "nutrient_group_id",
        "target_field": "nutrient_group"
      }
    }
  ]
}

PUT nutrient_names_and_groups

PUT /nutrient_names_and_groups/_mapping
{
  "properties" : {
    "nutrient_code" : {
      "type" : "long"
    },
    "nutrient_decimals" : {
      "type" : "long"
    },
    "nutrient_group_id" : {
      "type" : "long"
    },
    "nutrient_name" : {
      "type" : "text",
      "fields" : {
        "keyword" : {
          "type" : "keyword",
          "ignore_above" : 256
        }
      }
    },
    "nutrient_name_id" : {
      "type" : "long"
    },
    "nutrient_symbol" : {
      "type" : "text",
      "fields" : {
        "keyword" : {
          "type" : "keyword",
          "ignore_above" : 256
        }
      }
    },
    "nutrient_web_name" : {
      "type" : "text",
      "fields" : {
        "keyword" : {
          "type" : "keyword",
          "ignore_above" : 256
        }
      }
    },
    "nutrient_web_order" : {
      "type" : "long"
    },
    "tagname" : {
      "type" : "text",
      "fields" : {
        "keyword" : {
          "type" : "keyword",
          "ignore_above" : 256
        }
      }
    },
    "unit" : {
      "type" : "text",
      "fields" : {
        "keyword" : {
          "type" : "keyword",
          "ignore_above" : 256
        }
      }
    },
    "nutrient_group": {
      "properties" : {
          "nutrient_group_id" : {
            "type" : "long"
          },
          "nutrient_group_name" : {
            "type" : "text",
            "fields" : {
              "keyword" : {
                "type" : "keyword",
                "ignore_above" : 256
              }
            }
          },
          "nutrient_group_order" : {
            "type" : "long"
          }
        }
    }
  }
}

POST _reindex?slices=4&refresh&wait_for_completion=false
{
    "source": {
        "index": "nutrient_names"
    },
    "dest": {
        "index": "nutrient_names_and_groups",
        "pipeline": "nutrient_groups_pipeline"
    }
}
GET nutrient_names_and_groups/_search
#################################################################
# Enrich nutrient_amounts with:
# nutrient_names_and_groups
# and nutrient_sources
################################################################
PUT /_enrich/policy/nutrient_names_and_groups_policy
{ 
  "match": {
    "indices": [ 
      "nutrient_names_and_groups"
    ], 
    "match_field": "nutrient_name_id",
    "enrich_fields": [
      "nutrient_code",
			"nutrient_symbol",
			"nutrient_name",
			"unit",
			"tagname",
			"nutrient_decimals",
			"nutrient_web_order",
			"nutrient_web_name",
			"nutrient_group.nutrient_group_id",
			"nutrient_group.nutrient_group_name",
			"nutrient_group.nutrient_group_order"
    ] 
  }
}
POST /_enrich/policy/nutrient_names_and_groups_policy/_execute

PUT _ingest/pipeline/nutrient_names_and_groups_pipeline
{
  "processors": [
    {
      "enrich": {
        "policy_name": "nutrient_names_and_groups_policy",
        "field": "nutrient_name_id",
        "target_field": "name"
      }
    }
  ]
}

PUT nutrient_amounts_names_and_groups

PUT /nutrient_amounts_names_and_groups/_mapping
{
  "properties" : {
    "food_code" : {
      "type" : "long"
    },
    "number_observation" : {
      "type" : "long"
    },
    "nutrient_name_id" : {
      "type" : "long"
    },
    "nutrient_source_id" : {
      "type" : "long"
    },
    "nutrient_value" : {
      "type" : "float"
    },
    "nutrient_web_name" : {
      "type" : "text",
      "fields" : {
        "keyword" : {
          "type" : "keyword",
          "ignore_above" : 256
        }
      }
    },
    "standard_error" : {
      "type" : "long"
    },
    "name" : {
      "properties" : {
        "nutrient_code" : {
          "type" : "long"
        },
        "nutrient_decimals" : {
          "type" : "long"
        },
        "nutrient_group_id" : {
          "type" : "long"
        },
        "nutrient_name" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "nutrient_name_id" : {
          "type" : "long"
        },
        "nutrient_symbol" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "nutrient_web_name" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "nutrient_web_order" : {
          "type" : "long"
        },
        "tagname" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "unit" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "nutrient_group": {
          "properties" : {
              "nutrient_group_id" : {
                "type" : "long"
              },
              "nutrient_group_name" : {
                "type" : "text",
                "fields" : {
                  "keyword" : {
                    "type" : "keyword",
                    "ignore_above" : 256
                  }
                }
              },
              "nutrient_group_order" : {
                "type" : "long"
              }
            }
        }
      }
    }
  }
}

POST _reindex?refresh&wait_for_completion=false
{
    "source": {
        "index": "nutrient_amounts"
    },
    "dest": {
        "index": "nutrient_amounts_names_and_groups",
        "pipeline": "nutrient_names_and_groups_pipeline"
    }
}
GET nutrient_amounts_names_and_groups/_search
{
  "track_total_hits": true
}
##############################################
PUT /_enrich/policy/nutrient_source_policy
{ 
  "match": {
    "indices": [
      "nutrient_sources"
    ],
    "match_field": "nutrient_source_id",
    "enrich_fields": [
      "nutrient_source_id",
      "nutrient_source_description",
			"nutrient_source_code"
    ]
  }
}
POST /_enrich/policy/nutrient_source_policy/_execute

PUT _ingest/pipeline/nutrient_sources_pipeline
{
  "processors": [
    {
      "enrich": {
        "policy_name": "nutrient_source_policy",
        "field": "nutrient_source_id",
        "target_field": "source"
      }
    }
  ]
}

PUT nutrients

POST _reindex?refresh&wait_for_completion=false
{
    "source": {
        "index": "nutrient_amounts_names_and_groups"
    },
    "dest": {
        "index": "nutrients",
        "pipeline": "nutrient_sources_pipeline"
    }
}
GET nutrients/_search
{
  "track_total_hits": true
}
################################################################
# Reindex food_name My method1: Food inside of Nutrient
################################################################
PUT /_enrich/policy/food_policy
{ 
  "match": {
    "indices": [
      "foods"
    ],
    "match_field": "food_code",
    "enrich_fields": [
      "food_code",
      "food_description"
    ]
  }
}
POST /_enrich/policy/food_policy/_execute

PUT _ingest/pipeline/foods_pipeline
{
  "processors": [
    {
      "enrich": {
        "policy_name": "food_policy",
        "field": "food_code",
        "target_field": "food"
      }
    }
  ]
}

PUT nutrients_enriched

POST _reindex?refresh&wait_for_completion=false
{
    "source": {
        "index": "nutrients"
    },
    "dest": {
        "index": "nutrients_enriched",
        "pipeline": "foods_pipeline"
    }
}

GET nutrients_enriched/_search
{
  "track_total_hits": true
}
################################################################
# Reindex food_name My method1: Nutrients inside of Food
################################################################
DELETE /_enrich/policy/nutrients_policy
PUT /_enrich/policy/nutrients_policy
{
    "match": {
        "indices": "nutrients",
        "match_field": "food_code",
        "enrich_fields": [
          "food_code",
          "number_observation",
          "nutrient_name_id",
          "nutrient_source_id",
          "nutrient_value",
          "nutrient_web_name",
          "standard_error",
          "name.nutrient_code",
          "name.nutrient_decimals",
          "name.nutrient_name",
          "name.nutrient_name_id",
          "name.nutrient_symbol",
          "name.nutrient_web_name",
          "name.nutrient_web_order",
          "name.tagname",
          "name.unit",
          "name.nutrient_group.nutrient_group_id",
          "name.nutrient_group.nutrient_group_name",
          "name.nutrient_group.nutrient_group_order",
          "source.nutrient_source_code",
          "source.nutrient_source_description",
          "source.nutrient_source_id"
        ],
        "query": {
          "range":{
            "nutrient_value" : {
              "gt": 0
            }
          }
        }
    }
}


POST /_enrich/policy/nutrients_policy/_execute?wait_for_completion=false
GET _tasks/yNGZGxQJRUiNjUEOmlcvww:53939

DELETE _ingest/pipeline/nutrients_pipeline
PUT _ingest/pipeline/nutrients_pipeline
{
	"processors": [
		{
			"enrich": {
			  "policy_name" : "nutrients_policy",
				"field": "food_code",
				"target_field":"nutrients",
				"max_matches": 128
			}
		}
	]
}


PUT foods_enriched
DELETE foods_enriched

PUT foods_enriched
{
  "mappings": {
    "properties" : {
        "food_code" : {"type" : "long"},
        "food_description" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "nutrients": {
            "type": "nested",
            "include_in_parent": true,
            "include_in_root": true, 
            "properties":{
                "food_code" : {"type" : "long"},
                "number_observation" : {"type" : "long"},
                "nutrient_name_id" : {"type" : "long"},
                "nutrient_source_id" : {"type" : "long"},
                "nutrient_value" : {"type" : "float"},
                "nutrient_web_name" : {
                  "type" : "text",
                  "fields" : {
                    "keyword" : {
                      "type" : "keyword",
                      "ignore_above" : 256
                    }
                  }
                },
                "standard_error" : {"type" : "long"},
                "source" : {
                    "properties" : {
                        "nutrient_source_code" : {"type" : "long"},
                        "nutrient_source_description" : {
                          "type" : "text",
                          "fields" : {
                            "keyword" : {
                              "type" : "keyword",
                              "ignore_above" : 256
                            }
                          }
                        },
                        "nutrient_source_id" : {"type" : "long"}
                    }
                },
                "name" : {
                    "properties" : {
                        "nutrient_code" : {"type" : "long"},
                        "nutrient_decimals" : {"type" : "long"},
                        "nutrient_name" : {
                          "type" : "text",
                          "fields" : {
                            "keyword" : {
                              "type" : "keyword",
                              "ignore_above" : 256
                            }
                          }
                        },
                        "nutrient_name_id" : {"type" : "long"},
                        "nutrient_symbol" : {
                          "type" : "text",
                          "fields" : {
                            "keyword" : {
                              "type" : "keyword",
                              "ignore_above" : 256
                            }
                          }
                        },
                        "nutrient_web_name" : {
                          "type" : "text",
                          "fields" : {
                            "keyword" : {
                              "type" : "keyword",
                              "ignore_above" : 256
                            }
                          }
                        },
                        "nutrient_web_order" : {"type" : "long"},
                        "tagname" : {
                          "type" : "text",
                          "fields" : {
                            "keyword" : {
                              "type" : "keyword",
                              "ignore_above" : 256
                            }
                          }
                        },
                        "unit" : {
                          "type" : "text",
                          "fields" : {
                            "keyword" : {
                              "type" : "keyword",
                              "ignore_above" : 256
                            }
                          }
                        },
                        "nutrient_group" : {
                          "properties" : {
                            "nutrient_group_id" : {"type" : "long"},
                            "nutrient_group_name" : {
                              "type" : "text",
                              "fields" : {
                                "keyword" : {
                                  "type" : "keyword",
                                  "ignore_above" : 256
                                }
                              }
                            },
                            "nutrient_group_order" : {"type" : "long"}
                          }
                        }
                    }
                }
            }
        }
    }
  }
}


POST foods_enriched/_doc?pipeline=nutrients_pipeline
{"food_code":1256,"food_description":"Juste des céréales"}
GET foods_enriched/_search


POST _reindex?refresh&wait_for_completion=false
{
    "source": {
        "index": "foods"
    },
    "dest": {
        "index": "foods_enriched",
        "pipeline": "nutrients_pipeline"
    }
}

GET _tasks/yNGZGxQJRUiNjUEOmlcvww:28269

GET foods_enriched/_search

# ########################################################################
# Convertir tous les grammes en micro-grammes
POST nutrients_enriched/_update_by_query?wait_for_completion=false
{
  "script": {
    "source": "ctx._source.name.unit = params.unit; ctx._source.nutrient_value = ctx._source.nutrient_value * 1000000 ",
    "lang": "painless",
    "params": {
      "unit": "µg"
    }
  },
  "query": {
    "term":{
      "name.unit": "g"
    }
  }
}

# Convertir tous les milli-grammes en micro-grammes
POST nutrients_enriched/_update_by_query?wait_for_completion=false
{
  "script": {
    "source": "ctx._source.name.unit = params.unit; ctx._source.nutrient_value = ctx._source.nutrient_value * 1000 ",
    "lang": "painless",
    "params": {
      "unit": "µg"
    }
  },
  "query": {
    "term":{
      "name.unit": "mg"
    }
  }
}
###########################################################################
# Ajouter des nouveaux champs dans foods_enriched
################################################################################
GET foods_enriched/_search
POST foods_enriched/_update_by_query
{
  "script": {
    "source": "ctx._source.tags.add(params.nutrient_value)",
    "lang": "painless"
  },
  "query": { 
    "match_all": {}
  }
}

GET foods_enriched/_search
{
  "query": {
    "match_all": {}
  }
}